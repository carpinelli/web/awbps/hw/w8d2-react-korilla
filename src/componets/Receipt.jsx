const Order = function({ order })
{
  const objectMap = function(object, mapFunction)
  {
    return Object.fromEntries(Object.entries(object).map(([key, value], index) =>
    {
      return [key, mapFunction(value, key, index)];
    }))
  };

  const capitalize = ([firstLetter, ...rest]) =>
  {
    return firstLetter.toUpperCase() + rest.join('');
  };


  const orderEnumerableMembers = Object.keys(order);

  return (
    <ul className="order">
      {orderEnumerableMembers.map((orderPart) =>
        {
          if (orderPart === "toppings")
          {
            return null;
          }
          return (
            <li key={crypto.randomUUID()}>
              <h4><span className="purple">{`${capitalize(orderPart)}: `}</span>{order[orderPart]}</h4>
            </li>
          )
        })}
    </ul>
  );
}

const Receipt = function({ receipt, onClick })
{
  const handleReceiptClick = function()
  {
    onClick(receipt.id);
  };


  return (
    <section key={crypto.randomUUID()} className="receipt" onClick={handleReceiptClick}>
      <h2>{receipt.person}</h2>
      <Order order={receipt.order} />
    </section>
  );
};

export default Receipt;

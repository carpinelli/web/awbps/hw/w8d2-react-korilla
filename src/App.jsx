import "./styles.css";
import { useState } from "react";
import Receipt from "./componets/Receipt";

const receipts = [
  {
    person: "Karolin",
    order: {
      main: "Burrito",
      protein: "Organic Tofu",
      rice: "Purple Rice",
      sauce: "Green Crack",
      toppings: ["Baby Bok Choy", "Cucumber Kimchi"],
      drink: "Korchata",
      cost: 22
    },
    paid: false
  },
  {
    person: "Mark",
    order: {
      main: "Rice Bowl",
      protein: "Ginger Soy Chix",
      rice: "Sticky Rice",
      sauce: "Korilla",
      toppings: ["Yuzu Pickled Sweet Pepper", "Kale"],
      drink: "Korchata",
      cost: 19
    },
    paid: false
  },
  {
    person: "Matt",
    order: {
      main: "Salad Bowl",
      protein: "Organic Tofu",
      rice: "none",
      sauce: "Korilla",
      toppings: ["Blue Potato Salad", "Pico De Gallo", "Red Kimchi"],
      drink: "Sparkling Blood Orange Soda",
      cost: 20
    },
    paid: true
  }
];
const receiptsWithIds = receipts.map((dataItem) =>
{
  return {...dataItem, id: crypto.randomUUID()}
});

const App = function() {
  const [stateReceipts, setReceipts] = useState(receiptsWithIds)

  const handleClick = function(id)
  {
    console.log(id);
    setReceipts((previousReceipts) =>
    {
      return previousReceipts.map((receipt) =>
      {
        return receipt.id === id ? {...receipt, paid: !receipt.paid} : receipt;
      });
    });

    return null;
  };


  return (
    <div className="App">
      <h1>Korilla</h1>
      <div className="container">
        {stateReceipts.map((receipt) =>
        {
          if (!receipt.paid)
          {
            return <Receipt
                    key={receipt.id}
                    receipt={receipt}
                    onClick={() => handleClick(receipt.id)}
                   />
          }
          return null;
        })}
            </div>
    </div>
  );
};

export default App;

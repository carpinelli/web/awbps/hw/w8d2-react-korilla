# BLANK

BLANK.

## Contact

Joseph Carpinelli <carpinelli.dev@protonmail.ch>.

## Building

BLANK.

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated, provided they align with the licenses and goals of the
project. Forking or copying is encouraged, just ensure to uphold the
licenses terms.

## License

This project is licensed under the GPL 3.0 license. Unless any libraries
used require otherwise, all contributions are made under the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
See the [LICENSE](LICENSE). In cases where a library is used that
requires a different or additional license, and that license is not
included, kindly inform the email provided in the "Contact" section.
Corrections will be made ASAP.

You can be released from the requirements of the above license by
purchasing a commercial license. Buying such a license is mandatory
if you want to modify or otherwise use the software for commercial
activities involving the software without disclosing the source code
of your own applications. To purchase a commercial license, send an
email to <carpinelli.dev@protonmail.ch>.
